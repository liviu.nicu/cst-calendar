import {Component,State,Listen,Event,EventEmitter,Prop} from "@stencil/core";


@Component({
    tag:'cst-calendar',
    styleUrl:'cst-calendar.css'
})

export class CstCalendar{
    
    @Prop() config:any;
    
    @State() calendar;
    @State() displayLable;
    @State() currentMonth;
    @State() currentYear;
    @State() lable;
    @State() selectedDateObj;
    @State() settings;

    @Event() selectedDate:EventEmitter

    @Listen('onDateSelected') 
    dateSelected(day){
       this.selectedDateObj=new Date(this.currentYear,parseInt(this.currentMonth)-1,parseInt(day.detail))
        //console.log(new Date(this.currentYear,parseInt(this.currentMonth)-1,parseInt(day.detail)))
       this.selectedDate.emit(this.selectedDateObj)
    }   

    public monthName=['','JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
    public months=['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public monthsNumber=['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    public dayCount:any=['00','01','02','03','04','05','06','07','08','09',
                        '10','11','12','13','14','15','16','17','18','19',
                        '20','21','22','23','24','25','26','27','28','29',
                        '30'];

    public weekDaysName:any=['SUN','MON','TUE','WED','THU','FRI','SAT'];

    switchMounth=function(next,month,year){
        let curr=this.lable.split(" ");
       
        curr[0]=parseInt(curr[0],10);
        let tempYear=parseInt(curr[1],10);
  
  
        if(next==true){
          if(curr[0]===11){
            month=0;
            year=tempYear+1
          }else {
            month=curr[0]+1;
            year=tempYear;
          }
        }
      if(next==false){
          if(curr[0]===0){
            month=11;
            year=tempYear-1
          }else{
            month=curr[0]-1
            year=tempYear;
          }
        }
  
        if(next===true || next===false){
          this.lable=month+" "+year;
        }
  
        this.displayLable=this.months[parseInt(month)]+" "+year;
        this.currentMonth=this.monthsNumber[parseInt(month)];
        this.currentYear=year;
        this.createCal(month,year);
      }

      
  
      createCal=function(month,year){
        let day=1,i=0,j=0,haveDays=true;
        let startDay=new Date(year,month,day).getDay();
        if(startDay==-1){
          startDay=6;
        }
        let daysInMounth=[31,(((year%4==0)&&(year%100!==0))||(year%400===0))?29:28,31,30,31,30,31,31,30,31,30,31];
        let calendar=[];
  
        while(haveDays){
          calendar[i]=[];
          for(j=0;j<7;j++){
            if(i===0){
  
              if(j===startDay){
                calendar[i][j]=day++;
                startDay++;
              }else{
                calendar[i][j]="";
              }
            }else if(day <= daysInMounth[month]){
                calendar[i][j]=day++;
            }else{
              calendar[i][j]="";
              haveDays=false;
            }
  
            if(day>daysInMounth[month]){
              haveDays=false;
            }
          }
          i++;
        }
        this.calendar=calendar;
      }


    
    next=function(){
      this.switchMounth(true);
    }
    prev=function(){
      this.switchMounth(false);
    }
  
    componentWillLoad(){
        

        this.settings={
          "selectedDates":this.config.selectedDates.length?this.config.selectedDates:[],
          "range":this.config.range?true:false
        }

        console.log(this.settings)
        
        this.lable=new Date().getMonth()+" "+new Date().getFullYear();
        this.switchMounth(null,new Date().getMonth(),new Date().getFullYear());
    }

    render(){
      
        return <div>
        <table class="table-controls">
            <tr>
                <td class="table-controls-buttons">
                    <button onClick={()=>this.prev()}>Prev</button><button onClick={()=>this.next()}>Next</button>
                </td>
                <td  class="table-controls-label">
                    {this.displayLable}
                </td>
                
            </tr>
        </table>
        <table class="table-body">
        <thead>
          <tr>
            {
            this.weekDaysName.map((item)=>{
                return <td>{item}</td>
            })
            }
            </tr>  
        </thead>
            {
                this.calendar.map((row)=>{
                    return <tr>
                        {row.map((item)=>{
                            let isPreselected=this.settings.selectedDates.filter((currentElement)=>{
                              //console.log(new Date(currentElement) + "------ "+ new Date(this.currentYear,this.currentMonth-1,item))
                              return new Date(currentElement).getTime()===new Date(this.currentYear,this.currentMonth,item).getTime()})
                            
                            return <td><cst-calendar-number 
                            value={item} 
                            month={this.currentMonth} 
                            year={this.currentYear} 
                            selected={this.selectedDateObj}
                            settings={this.settings}
                            isPreselected={isPreselected.length?true:false}
                            ></cst-calendar-number></td>
                        })}
                    </tr>
                })
            } 
        </table>

        </div>
        
    }
}