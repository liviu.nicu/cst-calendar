import {Component,Prop,Event,EventEmitter,State} from "@stencil/core";

@Component({
    tag:'cst-calendar-number',
    styleUrl:'cst-calendar-number.css'
})

export class CstCalendarNumber{
    @Prop() value:number;
    @Prop() month:number;
    @Prop() year:number;
    @Prop() selected:any;
    @Prop() settings:any;
    @Prop() isPreselected:boolean;

    @Event() onDateSelected:EventEmitter;
    @State() daySelected:number;
    
    selectDay=function(day){
        this.daySelected=day;
        this.onDateSelected.emit(day)
    }
    
    render(){
        let isToday=((this.value==new Date().getDate()) && (this.month-1==new Date().getMonth()) && (this.year==new Date().getFullYear()))
        let isSelected=((this.daySelected==new Date(this.selected).getDate()) && (this.month-1==new Date().getMonth()) && (this.year==new Date().getFullYear()))
        let btnClass=this.isPreselected?"isPreselected":isSelected?"isSelected":this.value?"btn":"btn-inactive"
        //console.log(this.isPreselected)
        return <div onClick={this.value?this.selectDay.bind(this,this.value):null} class={btnClass}>
                    <span class={isToday?'today':""}> 
                        {this.value?this.value:''}
                    </span>
                </div>
    }
}