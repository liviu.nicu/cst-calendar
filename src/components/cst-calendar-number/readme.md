# cst-calendar-number



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description | Type      | Default     |
| --------------- | ---------------- | ----------- | --------- | ----------- |
| `isPreselected` | `is-preselected` |             | `boolean` | `undefined` |
| `month`         | `month`          |             | `number`  | `undefined` |
| `selected`      | `selected`       |             | `any`     | `undefined` |
| `settings`      | `settings`       |             | `any`     | `undefined` |
| `value`         | `value`          |             | `number`  | `undefined` |
| `year`          | `year`           |             | `number`  | `undefined` |


## Events

| Event            | Description | Type                |
| ---------------- | ----------- | ------------------- |
| `onDateSelected` |             | `CustomEvent<void>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
