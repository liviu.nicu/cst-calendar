
// cstCalendar: Custom Elements Define Library, ES Module/es2017 Target

import { defineCustomElement } from './cstcalendar.core.js';
import { COMPONENTS } from './cstcalendar.components.js';

export function defineCustomElements(win, opts) {
  return defineCustomElement(win, COMPONENTS, opts);
}
