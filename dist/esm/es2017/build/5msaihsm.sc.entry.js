import { h } from '../cstcalendar.core.js';

class CstCalendar {
    constructor() {
        this.monthName = ['', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.monthsNumber = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        this.dayCount = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09',
            '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
            '20', '21', '22', '23', '24', '25', '26', '27', '28', '29',
            '30'];
        this.weekDaysName = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
        this.switchMounth = function (next, month, year) {
            let curr = this.lable.split(" ");
            curr[0] = parseInt(curr[0], 10);
            let tempYear = parseInt(curr[1], 10);
            if (next == true) {
                if (curr[0] === 11) {
                    month = 0;
                    year = tempYear + 1;
                }
                else {
                    month = curr[0] + 1;
                    year = tempYear;
                }
            }
            if (next == false) {
                if (curr[0] === 0) {
                    month = 11;
                    year = tempYear - 1;
                }
                else {
                    month = curr[0] - 1;
                    year = tempYear;
                }
            }
            if (next === true || next === false) {
                this.lable = month + " " + year;
            }
            this.displayLable = this.months[parseInt(month)] + " " + year;
            this.currentMonth = this.monthsNumber[parseInt(month)];
            this.currentYear = year;
            this.createCal(month, year);
        };
        this.createCal = function (month, year) {
            let day = 1, i = 0, j = 0, haveDays = true;
            let startDay = new Date(year, month, day).getDay();
            if (startDay == -1) {
                startDay = 6;
            }
            let daysInMounth = [31, (((year % 4 == 0) && (year % 100 !== 0)) || (year % 400 === 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            let calendar = [];
            while (haveDays) {
                calendar[i] = [];
                for (j = 0; j < 7; j++) {
                    if (i === 0) {
                        if (j === startDay) {
                            calendar[i][j] = day++;
                            startDay++;
                        }
                        else {
                            calendar[i][j] = "";
                        }
                    }
                    else if (day <= daysInMounth[month]) {
                        calendar[i][j] = day++;
                    }
                    else {
                        calendar[i][j] = "";
                        haveDays = false;
                    }
                    if (day > daysInMounth[month]) {
                        haveDays = false;
                    }
                }
                i++;
            }
            this.calendar = calendar;
        };
        this.next = function () {
            this.switchMounth(true);
        };
        this.prev = function () {
            this.switchMounth(false);
        };
    }
    dateSelected(day) {
        this.selectedDateObj = new Date(this.currentYear, parseInt(this.currentMonth) - 1, parseInt(day.detail));
        this.selectedDate.emit(this.selectedDateObj);
    }
    componentWillLoad() {
        this.settings = {
            "selectedDates": this.config.selectedDates.length ? this.config.selectedDates : [],
            "range": this.config.range ? true : false
        };
        console.log(this.settings);
        this.lable = new Date().getMonth() + " " + new Date().getFullYear();
        this.switchMounth(null, new Date().getMonth(), new Date().getFullYear());
    }
    render() {
        return h("div", null,
            h("table", { class: "table-controls" },
                h("tr", null,
                    h("td", { class: "table-controls-buttons" },
                        h("button", { onClick: () => this.prev() }, "Prev"),
                        h("button", { onClick: () => this.next() }, "Next")),
                    h("td", { class: "table-controls-label" }, this.displayLable))),
            h("table", { class: "table-body" },
                h("thead", null,
                    h("tr", null, this.weekDaysName.map((item) => {
                        return h("td", null, item);
                    }))),
                this.calendar.map((row) => {
                    return h("tr", null, row.map((item) => {
                        let isPreselected = this.settings.selectedDates.filter((currentElement) => {
                            return new Date(currentElement).getTime() === new Date(this.currentYear, this.currentMonth, item).getTime();
                        });
                        return h("td", null,
                            h("cst-calendar-number", { value: item, month: this.currentMonth, year: this.currentYear, selected: this.selectedDateObj, settings: this.settings, isPreselected: isPreselected.length ? true : false }));
                    }));
                })));
    }
    static get is() { return "cst-calendar"; }
    static get properties() { return {
        "calendar": {
            "state": true
        },
        "config": {
            "type": "Any",
            "attr": "config"
        },
        "currentMonth": {
            "state": true
        },
        "currentYear": {
            "state": true
        },
        "displayLable": {
            "state": true
        },
        "lable": {
            "state": true
        },
        "selectedDateObj": {
            "state": true
        },
        "settings": {
            "state": true
        }
    }; }
    static get events() { return [{
            "name": "selectedDate",
            "method": "selectedDate",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "onDateSelected",
            "method": "dateSelected"
        }]; }
    static get style() { return "cst-calendar{font-family:Arial,Helvetica,sans-serif}cst-calendar .table-body{width:100%}cst-calendar .table-body>thead>tr>td{position:relative;display:table-cell;text-align:center;padding:1rem 3.5rem;background:#64b4e5;color:#fff;font-family:inherit;font-size:.7em;font-weight:300;line-height:normal;border:0;-webkit-appearance:none;-moz-appearance:none;appearance:none}cst-calendar .table-controls{width:100%}cst-calendar .table-controls .table-controls-buttons{width:20%}cst-calendar .table-controls .table-controls-label{display:block;padding:1rem;background:#fff;color:#64b4e5;font-weight:800;-webkit-box-shadow:0 0 .2px rgba(0,0,0,.1);box-shadow:0 0 .2px rgba(0,0,0,.1);text-align:center;text-decoration:underline;text-transform:uppercase}cst-calendar .table-controls .table-controls-label,cst-calendar button{position:relative;margin:0;font-family:inherit;font-size:1rem;line-height:normal;border:0;border-radius:.4rem;-webkit-appearance:none;-moz-appearance:none;appearance:none;cursor:pointer;-webkit-transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,box-shadow .25s;transition:background .25s,box-shadow .25s,-webkit-box-shadow .25s}cst-calendar button{display:inline-block;padding:1rem 3.5rem;background:#64b4e5;color:#fff;font-weight:300;-webkit-box-shadow:-1px 1px 8px rgba(0,0,0,.4);box-shadow:-1px 1px 8px rgba(0,0,0,.4)}cst-calendar button:hover{background:#97cdef;-webkit-box-shadow:-2px 2px 16px rgba(0,0,0,.6);box-shadow:-2px 2px 16px rgba(0,0,0,.6)}cst-calendar button:active,cst-calendar button:focus{outline:none}cst-calendar button:active{-webkit-box-shadow:-4px 4px 24px rgba(0,0,0,.8);box-shadow:-4px 4px 24px rgba(0,0,0,.8)}"; }
}

class CstCalendarNumber {
    constructor() {
        this.selectDay = function (day) {
            this.daySelected = day;
            this.onDateSelected.emit(day);
        };
    }
    render() {
        let isToday = ((this.value == new Date().getDate()) && (this.month - 1 == new Date().getMonth()) && (this.year == new Date().getFullYear()));
        let isSelected = ((this.daySelected == new Date(this.selected).getDate()) && (this.month - 1 == new Date().getMonth()) && (this.year == new Date().getFullYear()));
        let btnClass = this.isPreselected ? "isPreselected" : isSelected ? "isSelected" : this.value ? "btn" : "btn-inactive";
        return h("div", { onClick: this.value ? this.selectDay.bind(this, this.value) : null, class: btnClass },
            h("span", { class: isToday ? 'today' : "" }, this.value ? this.value : ''));
    }
    static get is() { return "cst-calendar-number"; }
    static get properties() { return {
        "daySelected": {
            "state": true
        },
        "isPreselected": {
            "type": Boolean,
            "attr": "is-preselected"
        },
        "month": {
            "type": Number,
            "attr": "month"
        },
        "selected": {
            "type": "Any",
            "attr": "selected"
        },
        "settings": {
            "type": "Any",
            "attr": "settings"
        },
        "value": {
            "type": Number,
            "attr": "value"
        },
        "year": {
            "type": Number,
            "attr": "year"
        }
    }; }
    static get events() { return [{
            "name": "onDateSelected",
            "method": "onDateSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "cst-calendar-number .btn{position:relative;display:block;margin:0;padding:1rem;background:#fff;color:#64b4e5;font-family:inherit;font-size:1.6rem;font-weight:300;line-height:normal;border:0;border-radius:.4rem;-webkit-box-shadow:0 0 .2px rgba(0,0,0,.1);box-shadow:0 0 .2px rgba(0,0,0,.1);-webkit-appearance:none;-moz-appearance:none;appearance:none;cursor:pointer;-webkit-transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,box-shadow .25s;transition:background .25s,box-shadow .25s,-webkit-box-shadow .25s;min-height:100px}cst-calendar-number .today{color:orange;text-decoration:underline}cst-calendar-number .isSelected{background:#64b4e5!important;color:#fff}cst-calendar-number .isPreselected,cst-calendar-number .isSelected{position:relative;display:block;margin:0;padding:1rem;font-family:inherit;font-size:1.6rem;font-weight:300;line-height:normal;border:0;border-radius:.4rem;-webkit-box-shadow:0 0 .2px rgba(0,0,0,.1);box-shadow:0 0 .2px rgba(0,0,0,.1);-webkit-appearance:none;-moz-appearance:none;appearance:none;cursor:pointer;-webkit-transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,box-shadow .25s;transition:background .25s,box-shadow .25s,-webkit-box-shadow .25s;min-height:100px}cst-calendar-number .isPreselected{background:#c9d0d4;color:#64b4e5}cst-calendar-number .btn-inactive{position:relative;display:block;margin:0;padding:1rem;background:#f0f0f0;color:#64b4e5;font-family:inherit;font-size:1.6rem;font-weight:300;line-height:normal;border:0;border-radius:.4rem;-webkit-box-shadow:0 0 .2px rgba(0,0,0,.1);box-shadow:0 0 .2px rgba(0,0,0,.1);-webkit-appearance:none;-moz-appearance:none;appearance:none;-webkit-transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,-webkit-box-shadow .25s;transition:background .25s,box-shadow .25s;transition:background .25s,box-shadow .25s,-webkit-box-shadow .25s;min-height:100px}cst-calendar-number>.btn:hover{background:#f0f0f0;-webkit-box-shadow:0 0 0 rgba(0,0,0,.2);box-shadow:0 0 0 rgba(0,0,0,.2)}cst-calendar-number>.btn:active,cst-calendar-number>.btn:focus{outline:none}cst-calendar-number>.btn:active{-webkit-box-shadow:-4px 4px 24px rgba(0,0,0,.2);box-shadow:-4px 4px 24px rgba(0,0,0,.2)}"; }
}

export { CstCalendar, CstCalendarNumber };
