export class CstCalendar {
    constructor() {
        this.monthName = ['', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.monthsNumber = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        this.dayCount = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09',
            '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
            '20', '21', '22', '23', '24', '25', '26', '27', '28', '29',
            '30'];
        this.weekDaysName = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
        this.switchMounth = function (next, month, year) {
            let curr = this.lable.split(" ");
            curr[0] = parseInt(curr[0], 10);
            let tempYear = parseInt(curr[1], 10);
            if (next == true) {
                if (curr[0] === 11) {
                    month = 0;
                    year = tempYear + 1;
                }
                else {
                    month = curr[0] + 1;
                    year = tempYear;
                }
            }
            if (next == false) {
                if (curr[0] === 0) {
                    month = 11;
                    year = tempYear - 1;
                }
                else {
                    month = curr[0] - 1;
                    year = tempYear;
                }
            }
            if (next === true || next === false) {
                this.lable = month + " " + year;
            }
            this.displayLable = this.months[parseInt(month)] + " " + year;
            this.currentMonth = this.monthsNumber[parseInt(month)];
            this.currentYear = year;
            this.createCal(month, year);
        };
        this.createCal = function (month, year) {
            let day = 1, i = 0, j = 0, haveDays = true;
            let startDay = new Date(year, month, day).getDay();
            if (startDay == -1) {
                startDay = 6;
            }
            let daysInMounth = [31, (((year % 4 == 0) && (year % 100 !== 0)) || (year % 400 === 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            let calendar = [];
            while (haveDays) {
                calendar[i] = [];
                for (j = 0; j < 7; j++) {
                    if (i === 0) {
                        if (j === startDay) {
                            calendar[i][j] = day++;
                            startDay++;
                        }
                        else {
                            calendar[i][j] = "";
                        }
                    }
                    else if (day <= daysInMounth[month]) {
                        calendar[i][j] = day++;
                    }
                    else {
                        calendar[i][j] = "";
                        haveDays = false;
                    }
                    if (day > daysInMounth[month]) {
                        haveDays = false;
                    }
                }
                i++;
            }
            this.calendar = calendar;
        };
        this.next = function () {
            this.switchMounth(true);
        };
        this.prev = function () {
            this.switchMounth(false);
        };
    }
    dateSelected(day) {
        this.selectedDateObj = new Date(this.currentYear, parseInt(this.currentMonth) - 1, parseInt(day.detail));
        this.selectedDate.emit(this.selectedDateObj);
    }
    componentWillLoad() {
        this.settings = {
            "selectedDates": this.config.selectedDates.length ? this.config.selectedDates : [],
            "range": this.config.range ? true : false
        };
        console.log(this.settings);
        this.lable = new Date().getMonth() + " " + new Date().getFullYear();
        this.switchMounth(null, new Date().getMonth(), new Date().getFullYear());
    }
    render() {
        return h("div", null,
            h("table", { class: "table-controls" },
                h("tr", null,
                    h("td", { class: "table-controls-buttons" },
                        h("button", { onClick: () => this.prev() }, "Prev"),
                        h("button", { onClick: () => this.next() }, "Next")),
                    h("td", { class: "table-controls-label" }, this.displayLable))),
            h("table", { class: "table-body" },
                h("thead", null,
                    h("tr", null, this.weekDaysName.map((item) => {
                        return h("td", null, item);
                    }))),
                this.calendar.map((row) => {
                    return h("tr", null, row.map((item) => {
                        let isPreselected = this.settings.selectedDates.filter((currentElement) => {
                            return new Date(currentElement).getTime() === new Date(this.currentYear, this.currentMonth, item).getTime();
                        });
                        return h("td", null,
                            h("cst-calendar-number", { value: item, month: this.currentMonth, year: this.currentYear, selected: this.selectedDateObj, settings: this.settings, isPreselected: isPreselected.length ? true : false }));
                    }));
                })));
    }
    static get is() { return "cst-calendar"; }
    static get properties() { return {
        "calendar": {
            "state": true
        },
        "config": {
            "type": "Any",
            "attr": "config"
        },
        "currentMonth": {
            "state": true
        },
        "currentYear": {
            "state": true
        },
        "displayLable": {
            "state": true
        },
        "lable": {
            "state": true
        },
        "selectedDateObj": {
            "state": true
        },
        "settings": {
            "state": true
        }
    }; }
    static get events() { return [{
            "name": "selectedDate",
            "method": "selectedDate",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "onDateSelected",
            "method": "dateSelected"
        }]; }
    static get style() { return "/**style-placeholder:cst-calendar:**/"; }
}
