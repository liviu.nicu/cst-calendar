export class CstCalendarNumber {
    constructor() {
        this.selectDay = function (day) {
            this.daySelected = day;
            this.onDateSelected.emit(day);
        };
    }
    render() {
        let isToday = ((this.value == new Date().getDate()) && (this.month - 1 == new Date().getMonth()) && (this.year == new Date().getFullYear()));
        let isSelected = ((this.daySelected == new Date(this.selected).getDate()) && (this.month - 1 == new Date().getMonth()) && (this.year == new Date().getFullYear()));
        let btnClass = this.isPreselected ? "isPreselected" : isSelected ? "isSelected" : this.value ? "btn" : "btn-inactive";
        return h("div", { onClick: this.value ? this.selectDay.bind(this, this.value) : null, class: btnClass },
            h("span", { class: isToday ? 'today' : "" }, this.value ? this.value : ''));
    }
    static get is() { return "cst-calendar-number"; }
    static get properties() { return {
        "daySelected": {
            "state": true
        },
        "isPreselected": {
            "type": Boolean,
            "attr": "is-preselected"
        },
        "month": {
            "type": Number,
            "attr": "month"
        },
        "selected": {
            "type": "Any",
            "attr": "selected"
        },
        "settings": {
            "type": "Any",
            "attr": "settings"
        },
        "value": {
            "type": Number,
            "attr": "value"
        },
        "year": {
            "type": Number,
            "attr": "year"
        }
    }; }
    static get events() { return [{
            "name": "onDateSelected",
            "method": "onDateSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "/**style-placeholder:cst-calendar-number:**/"; }
}
