import '../../stencil.core';
import { EventEmitter } from "../../stencil.core";
export declare class CstCalendarNumber {
    value: number;
    month: number;
    year: number;
    selected: any;
    settings: any;
    isPreselected: boolean;
    onDateSelected: EventEmitter;
    daySelected: number;
    selectDay: (day: any) => void;
    render(): JSX.Element;
}
