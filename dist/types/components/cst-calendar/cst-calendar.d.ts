import '../../stencil.core';
import { EventEmitter } from "../../stencil.core";
export declare class CstCalendar {
    config: any;
    calendar: any;
    displayLable: any;
    currentMonth: any;
    currentYear: any;
    lable: any;
    selectedDateObj: any;
    settings: any;
    selectedDate: EventEmitter;
    dateSelected(day: any): void;
    monthName: string[];
    months: string[];
    monthsNumber: string[];
    dayCount: any;
    weekDaysName: any;
    switchMounth: (next: any, month: any, year: any) => void;
    createCal: (month: any, year: any) => void;
    next: () => void;
    prev: () => void;
    componentWillLoad(): void;
    render(): JSX.Element;
}
