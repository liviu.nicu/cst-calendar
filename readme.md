![Built With Stencil](https://img.shields.io/badge/-Built%20With%20Stencil-16161d.svg?logo=data%3Aimage%2Fsvg%2Bxml%3Bbase64%2CPD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI%2BCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI%2BCgkuc3Qwe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU%2BCjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik00MjQuNywzNzMuOWMwLDM3LjYtNTUuMSw2OC42LTkyLjcsNjguNkgxODAuNGMtMzcuOSwwLTkyLjctMzAuNy05Mi43LTY4LjZ2LTMuNmgzMzYuOVYzNzMuOXoiLz4KPHBhdGggY2xhc3M9InN0MCIgZD0iTTQyNC43LDI5Mi4xSDE4MC40Yy0zNy42LDAtOTIuNy0zMS05Mi43LTY4LjZ2LTMuNkgzMzJjMzcuNiwwLDkyLjcsMzEsOTIuNyw2OC42VjI5Mi4xeiIvPgo8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNDI0LjcsMTQxLjdIODcuN3YtMy42YzAtMzcuNiw1NC44LTY4LjYsOTIuNy02OC42SDMzMmMzNy45LDAsOTIuNywzMC43LDkyLjcsNjguNlYxNDEuN3oiLz4KPC9zdmc%2BCg%3D%3D&colorA=16161d&style=flat-square)


Calendar component that you can use in any framework or with no framework at all. 


## Web component tag
```bash
<cst-calendar></cst-calendar>
```


### Angular quick start 
- Run `npm install cst-calendar --prefix ./src/assets`
- Put this script tag `<script src='assets/node_modules/cst-calendar/dist/cstcalendar.js'></script>` in the head of your index.html
- Including the `CUSTOM_ELEMENTS_SCHEMA` in the module allows the use of the web components in the HTML markup without the compiler producing errors. Here is an example of adding it to `AppModule`:
```bash
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule, SharedModule],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
```
- Then you can use the element anywhere in your template, JSX, html etc

### React quick start 
- Run `$ npm install cst-calendar --prefix ./public`
- Put this script tag `<script src='./node_modules/cst-calendar/dist/cstcalendar.js'></script>` in the head of your index.html
- Then you can use the element anywhere in your template, JSX, html etc

### Vanilla JS
- Run `$ npm install cst-calendar --save`
- Put this script tag `<script src='./node_modules/cst-calendar/dist/cstcalendar.js'></script>` in the head of your index.html
- Then you can use the element anywhere in your template

### In a stencil-starter app
- Run `npm install cst-calendar --save`
- Add an import to the npm packages `import cst-calendar;`
- Then you can use the element anywhere in your template, JSX, html etc

## Usage

### Angular
 ```bash
<cst-calendar (selectedDate)="getDate($event)" [config]='{"selectedDates":[new Date(2019,3,29),new Date(2019,3,30),new Date(2019,3,17)],"range":false}'></cst-calendar>
```
### React 
 ```bash
componentDidMount(){
    let calendarElement = document.querySelector('cst-calendar');
    calendarElement.addEventListener('selectedDate', event => { console.log(event) })
    calendarElement['config'] = {"selectedDates":[new Date(2019,3,29),new Date(2019,3,30),new Date(2019,3,17)],"range":false}
  }
  
  render() {
    return (
      <div className="App">
        <cst-calendar></cst-calendar>
      </div>
    );
  }
```

### Vanilla JS 
 ```bash
  <div className="App">
      <cst-calendar></cst-calendar>
  </div>

    <script>
      const element = document.querySelector('cst-calendar');
      element.addEventListener('selectedDate', event => { console.log(event) })
      element['config'] = {"selectedDates":[new Date(2019,3,29),new Date(2019,3,30),new Date(2019,3,17)],"range":false}
    </script>

```

